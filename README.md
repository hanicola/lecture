# Programming Techniques for Scientific Simulations I ([402-0811-00L](http://www.vorlesungsverzeichnis.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=140944&semkez=2020W&ansicht=KATALOGDATEN&lang=en))

### General information

**ETH Zurich to suspend most classroom teaching starting on 2 November**:

The course will take place in a **full online** format:

  * **Online**: Live stream over Zoom. All Meeting IDs can be found
    [here](http://www.sam.math.ethz.ch/~karoger/pt/hs2020/)
    (Login credentials were sent by Email on Thursday, October 29.
    If you were not enrolled in the course then, please
    send an email to the mailing list [below](#questions)).

  * **Lecture**: 13:45 - 15:30 with a 15 min break between 14:30 - 14:45.

  * **Exercises**: 15:45 - 17:30. First part will be in the lecture
    Z(r)oom. The interactive part will be in the individual PT1 team
    member Z(r)ooms. Simply join one, or choose the team member you
    want to discuss with.

  * **Exam preparation**: There will be several online Q&A sessions starting on
    January 15. The dates and times can be found
    [here](http://www.sam.math.ethz.ch/~karoger/pt/hs2020/).

**Should you have any issues with the "new" online format, please
 contact us through the mailing list (see [below](#questions)).**

The course is automatically (no edit) recorded and available here:

  * <https://video.ethz.ch/lectures/d-math/2020/autumn/402-0811-00L.html>

Unfortunately, the recording of lecture 11 (26.11.2020) is no available there.
You find it at the bottom of the page where all the Meeting IDs are published
above.

We kindly ask **all participants** to comply with the up-to-date latest
Coronavirus COVID-19 measures:

  * <https://ethz.ch/services/en/news-and-events/coronavirus.html>

### Summary

This lecture provides an overview of programming techniques for scientific
simulations.
The focus is on advanced C++ programming techniques and scientific software
libraries.
Based on an overview over the hardware components of PCs and supercomputers,
optimization methods for scientific simulation codes are explained.

### Questions

For questions or remarks we have a mailing list where you can reach us:
<pt1_hs20@sympa.ethz.ch >.
The mailing list closes for the holidays and re-opens on **January 3.**

## Lecture slides, script, exercises and solutions

Lecture slides, exercise sheets and solutions will be provided as part of this
git repository.
The [**lecture script**](https://gitlab.ethz.ch/pt1_hs20/script/-/blob/master/README.md)
is available in a [seperate repository](https://gitlab.ethz.ch/pt1_hs20/script).
There you can also find some additional [cheatsheets](https://gitlab.ethz.ch/pt1_hs20/script/-/tree/master/cheatsheets).

## Submission

If you want to receive feedback on your exercises, please push your solutions
to your own git repository before **Monday night** of the week after we hand
out the exercise sheet.
Then send a notification / request for correction email (possibly with
questions) to the mailing list.
Advanced users can utilise GitLab issues (make sure to tag all the assistants,
but not the professor, with their @name in the issue description).

Your exercise will then be corrected before the next exercise session.
Make sure to give *maintainer* access to the following people:
@karoger, @engelerp, @ilabarca, @msudwoj, @pollakg and @rworreby.

Of course, working in small groups is allowed (and even encouraged using a
collaborative workflow with `git` and `GitLab`).
However, please make sure that you understand every part of the group's
proposed solution (you will have to e.g., at the exam!).
If several group members submit the exercises, please indicate clearly in the
notification/request for correction email all the group members and indicate
which parts of the solution you would like to have looked at individually
(although we try to run data comparison tools carefully during the correction,
we may miss some individual solution of group members).

## Course confirmation (Testat)

For students needing the confirmation (Testat) for this course, we require
that 70% of the exercises have been solved reasonably (sinnvoll).
The submission deadline is every Wednesday midnight (Zurich time!).

Please announce that you want the confirmation (Testat) for this course
explicitly at the beginning of the semester. Contact us either in person or
through the mailing list.

## Exam information

* For general information, see the performance assessment tab in the course
  catalogue [here](http://www.vorlesungsverzeichnis.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?semkez=2020W&ansicht=LEISTUNGSKONTROLLE&lerneinheitId=140944&lang=en).

* The exam will have two parts: A written theoretical part, and a programming
  part that you will solve on the exam computers.

* The exam computers will run Fedora Linux, similar to those that you find in
  the computer rooms in the ETH main building.
  The system language of the computers is English.
  A list of the installed software can be found [here](https://www.ethz.ch/services/en/it-services/catalogue/managed-client/computer-rooms.html).
    * A virtual machine was created by staff of the Linux Examination Team
      from the LET. Although it is not exactly the same version as will
      be used at the exam, it is reasonably close so that you can get an
      impression. It is avalaible [here](http://idinstallprd.ethz.ch/img/Fedora31s.ova)
      (User: student, login-password: student) and can be run, e.g., with
      [VirtualBox](https://www.virtualbox.org/).

* By default, the keyboards will have the Swiss layout.
  There will be a poll for those who want to get a US keyboard instead.

* Provided on the computers are:
    * The full lecture repository
    * The C++ standard ([draft version](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3337.pdf))
    * An offline version of http://www.cppreference.com
      (See *Html book* https://en.cppreference.com/w/Cppreference:Archives)  
      We recommend that you try it out before the exam.
      Also note that the search function is absent: use the Index page and the
      search function of the browser.
    * An offline version of [the Python documentation](https://docs.python.org/3.8/index.html)
      (See https://docs.python.org/3.8/download.html)
      We recommend that you try it out before the exam.
    * As needed, offline versions of the documentation for Python libraries.
    * The script repository

* This is an open-book exam, which means that you can bring any written
  material (books, notes, printed code, ...).
  However, you may **not** use any digital devices (other than the exam
  computer) during the exam.

* Don't forget to bring your student card (Legi).

## Useful resources

### Literature

C/C++ primers and references:

* Stroustrup, "The C++ Programming Language", 2013.
  Available online within the ETH network [here](https://search.library.ethz.ch/permalink/f/823s1o/nebis_sfx3280000000036330).

* Kernighan and Ritchie, "C Programming Language", 1988.
  Available online within the ETH network [here](https://search.library.ethz.ch/permalink/f/13kse66/nebis_sfx3710000000220165).

Practical/Advanced C++ programming:

* Meyers, "Effective STL", 2001.
  Available online within the ETH network [here](https://search.library.ethz.ch/permalink/f/13kse66/nebis_sfx1000000000385265).

* Meyers, "Effective C : 50 Specific Ways to Improve Your Programs and
  Designs", 2005.
  Available online within the ETH network [here](https://search.library.ethz.ch/permalink/f/823s1o/ebi01_prod009837972).

* Meyers, "Effective Modern C++", 2014.
  Available online within the ETH network [here](https://search.library.ethz.ch/permalink/f/13kse66/nebis_sfx3800000000006955).

* Vandevoorde, Gregor, and Josuttis, "C++ Templates: The Complete Guide", 2017.
  Available online within the ETH network [here](https://search.library.ethz.ch/permalink/f/823s1o/nebis_sfx4100000000880644).


### Web resources

* [C++ reference](https://en.cppreference.com)
* [C++ standard](https://isocpp.org/)
* [C++ core guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
