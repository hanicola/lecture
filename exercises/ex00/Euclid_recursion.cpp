# include <iostream>

//Function declaration
int gcd_recursive(int a, int b);

int main() {
	int a, b, gcd;
	std::cout << "Give an integer a \n";
	std::cin >> a;
	std::cout << "Given an integer b \n";
	std::cin >> b;
	
	// swap numbers if a < b
	if (a<b){
		int i = a;
		a = b;
		b = i;
	}
	gcd = gcd_recursive(a, b);
	std::cout << gcd << "\n";
}

// Function definition
int gcd_recursive(int a, int b){
	if (b==0){
		return a;
	}
	else {
		return gcd_recursive(b, a%b);
	}
}