#include <iostream>

//function declaration
int gcd_loop(int a, int b);

int main() {
	int a, b, gcd;
	std::cout << "Give an integer a \n";
	std::cin >> a;
	std::cout << "Given an integer b \n";
	std::cin >> b;
	
	// swap numbers if a < b
	if (a<b){
		int i = a;
		a = b;
		b = i;
	}
	gcd = gcd_loop(a, b);
	std::cout << gcd << "\n";
}

//function definition
int gcd_loop(int a, int b){
	int t = 0;
	while( b != 0){
		t = b;
		b = a%b;
		a = t;
	}
	return a;
}