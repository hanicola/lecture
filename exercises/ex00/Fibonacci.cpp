#include <iostream>
#include <iomanip>
using namespace std;

int main(){
	cout << "Please give a positive integer" << endl;
	int n;
	cin >> n 	;
	int e;
	e = 0;

int f_2; 
int f_1; 
f_2 = 0;
f_1 = 1;

// Non-recursive
if (n == 1){
	e = f_1;
}

else {
	for (int i = 1; i < n; i++){
		e = f_1 + f_2;
		f_2 = f_1;
		f_1 = e;		
	}
}

cout << "The result (non-recursive) is: " << e << endl;

// Recursive
e = 0;
e = F_recursive(n, f_2, f_1, e);

cout << "The result (recursive) is: " << e << endl;

return 0;
}

int F_recursive(int n, int f_2, int f_1, int current){
if (n>0) { F_recursive(n-1, f_2, f_1, current)}
else {
	current = f_2 + f_1;
	f_2 = f_1;
	f_1 = current;
	return current;
}
}