/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 0.2
 * 2020-09-23
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <cassert>  // for assert
#include <cstdint>  // for std::uint64_t
#include <iostream> // for std::cin, std::cout

using fibonacci_t = std::uint64_t;

fibonacci_t fibonacci_recursive(fibonacci_t n) noexcept;
fibonacci_t fibonacci_loop(fibonacci_t n)      noexcept;

int main() {
	fibonacci_t n;
	
	std::cout << "Enter a non-negative integer n:\n";
	std::cin >> n;

	std::cout
		<< "fibonacci_loop(n)      = " << fibonacci_loop(n)      << '\n'
		<< "fibonacci_recursive(n) = " << fibonacci_recursive(n) << '\n'
	;
}

fibonacci_t fibonacci_recursive(fibonacci_t n) noexcept {
	assert(n <= 93);

	switch (n) {
		case 0:
			return 0;
		case 1:
			return 1;
		default:
			return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2);
	}
}

fibonacci_t fibonacci_loop(fibonacci_t n) noexcept {
	assert(n <= 93);

	if (n == 0) {
		return 0;
	}

	fibonacci_t previous = 0;
	fibonacci_t current  = 1;
	for (int i = 1; i < n; ++i) {
		fibonacci_t next = current + previous;
		previous = current;
		current = next;
	}

	return current;
}
