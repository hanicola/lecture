/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 0.1
 * 2020-09-23
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <cassert>  // for assert
#include <cstdint>  // for std::uint64_t
#include <iostream> // for std::cin, std::cout
#include <numeric>  // for std::gcd

using gcd_t = std::uint64_t;

gcd_t gcd_recursive(gcd_t a, gcd_t b) noexcept;
gcd_t gcd_loop(gcd_t a, gcd_t b) noexcept;

int main() {
	gcd_t a = 0;
	gcd_t b = 0;

	std::cout << "Enter two non-negative integers a and b:\n";
	std::cin >> a >> b;

	std::cout
		<< "gcd_recursive(a, b) = " << gcd_recursive(a, b) << '\n'
		<< "gcd_loop(a, b)      = " << gcd_loop(a, b)      << '\n'
// check that std::gcd is available (C++17)
// see https://en.cppreference.com/w/cpp/feature_test
#if defined(__cpp_lib_gcd_lcm) && __cpp_lib_gcd_lcm >= 201606L
		<< "std::gcd(a, b)      = " << std::gcd(a, b)      << '\n'
#endif
	;

}

gcd_t gcd_recursive(gcd_t a, gcd_t b) noexcept {
	if (a == 0 || b == 0) {
		return a + b;
	} else if (a > b) {
		return gcd_recursive(b, a % b);
	} else {
		return gcd_recursive(a, b % a);
	}
}

gcd_t gcd_loop(gcd_t a, gcd_t b) noexcept {
	while (a > 0 && b > 0) {
		if (a > b) {
			a = a % b;
		} else {
			b = b % a;
		}
	}

	return a + b;
}
