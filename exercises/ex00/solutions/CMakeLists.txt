# Programming Techniques for Scientific Simulations I
# HS 2020
# Exercise 0
# 2020-09-24
# Michal Sudwoj <msudwoj@student.ethz.ch>
cmake_minimum_required (VERSION 3.12)
project (ex00 LANGUAGES CXX)

set (CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_STANDARD_REQUIRED TRUE)
set (CMAKE_CXX_EXTENSIONS FALSE)

add_executable (fibonacci  fibonacci.cpp)
add_executable (gcd        gcd.cpp)
add_executable (merge_sort merge_sort.cpp)
