#include <iostream>
#include <iomanip>
using namespace std;

int main(){
  // numbers of iterest
  int n;
  int m;

  cout << "Please give an integer" << endl;
  cin >> n;
  cout << "Please give an integer" << endl;
  cin >> m;

  //greatest common divisor
  int d;

  //Euklid's algorithm (old) - no checking for valid input
  cout << "The greatest common divisor is: " << endl;
  if (m == 0)
    cout << n << endl;
  
  else
    while (n!=0)
      if (m>n)
        m = m-n;
      else
        n = n-m;
      cout << m << endl;
}