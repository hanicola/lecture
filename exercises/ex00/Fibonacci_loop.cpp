#include <iostream>

//function declaration
int fib_recursion(int n, int minus_two, int minus_one);

int main(){
	int n;
	std::cout << "Give a non-negative integer\n";
	std::cin >> n;
	
	int null = 0;
	int eins = 1;
	
	if (n==0){
		std::cout << null << "\n";
	}
	else if (n==1){
		std::cout << eins << "\n";
	}
	else{
		int minus_two = null;
		int minus_one = eins;
		std::cout << fib_recursion(n, minus_two, minus_one) << "\n";
	}
}

//function definition
int fib_recursion(int n, int minus_two, int minus_one){
	int current = 0;
	while (n >= 2){
		current = minus_two + minus_one;
		minus_two = minus_one;
		minus_one = current;
		n--;
	}
	return current;
}