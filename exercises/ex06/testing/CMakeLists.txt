cmake_minimum_required(VERSION 3.1)

project(fibonacci)

set(CMAKE_CXX_STANDARD 11)

add_subdirectory(src)

option(BUILD_TESTS "Build the tests." ON)

if (BUILD_TESTS)
  enable_testing()
  # Simple: Tests written in main.
  add_subdirectory(test_in_main)
  # Advanced: Tests written with the Catch test framework.
  add_subdirectory(test_with_catch)
endif (BUILD_TESTS)
