include_directories(${PROJECT_SOURCE_DIR}/src)

add_executable(fibonacci_test_main fibonacci_test.cpp)
target_link_libraries(fibonacci_test_main fibonacci)

add_test(NAME Fibonacci-Test-in-main COMMAND fibonacci_test_main)
