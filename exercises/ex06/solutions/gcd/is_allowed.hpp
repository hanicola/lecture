#ifndef IS_ALLOWED_HPP
#define IS_ALLOWED_HPP
#include <type_traits>

typedef std::integral_constant<bool, true> true_type;
typedef std::integral_constant<bool, false> false_type;

template <class T>
struct is_allowed : public false_type{};

template <>
struct is_allowed<short> : public true_type{};

template <>
struct is_allowed<unsigned short> : public true_type{};

template <>
struct is_allowed<int> : public true_type{};

template <>
struct is_allowed<unsigned int> : public true_type{};

template <>
struct is_allowed<long long> : public true_type{};

template <>
struct is_allowed<unsigned long long> : public true_type{};

// And so on, depending on what you want to have
#endif