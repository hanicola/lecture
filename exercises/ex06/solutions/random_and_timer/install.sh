# Abort on error (works only on bash and bash-compatible shells).
set -e

export INSTALL_LOCATION="${PWD}/install"

mkdir -p "${INSTALL_LOCATION}/lib"
mkdir -p "${INSTALL_LOCATION}/include"

# Install timer library
cmake -Htimer -Btimer/build -DCMAKE_INSTALL_PREFIX="${INSTALL_LOCATION}" -DCMAKE_BUILD_TYPE=Release
make -C timer/build
make -C timer/build install

# Install random library
cmake -Hrandom -Brandom/build -DCMAKE_INSTALL_PREFIX="${INSTALL_LOCATION}" -DCMAKE_BUILD_TYPE=Release
make -C random/build
make -C random/build install

# Compile benchmark
cmake -Hbenchmark -Bbenchmark/build -DCMAKE_INSTALL_PREFIX="${INSTALL_LOCATION}" -DCMAKE_BUILD_TYPE=Release
make -C benchmark/build
