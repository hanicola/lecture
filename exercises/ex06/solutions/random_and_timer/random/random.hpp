#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <cstdint>

class Generator {
public:
  using result_t = uint32_t;

  Generator(result_t seed=42);
  result_t generate();
  result_t max() const;

private:
  using impl_t = uint32_t;

  impl_t seed_;
  static const impl_t a_ = 1664525;
  static const impl_t c_ = 1013904223;
};

#endif // !defined RANDOM_HPP
