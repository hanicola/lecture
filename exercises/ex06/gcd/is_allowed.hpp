#ifndef IS_ALLOWED_HPP
#define IS_ALLOWED_HPP
#include <type_traits>

typedef std::integral_constant<bool, true> true_type;
typedef std::integral_constant<bool, false> false_type;

template <class T>
struct is_allowed : public false_type{};

// TODO add specialization for all possible types
template <>
struct is_allowed<int> : public true_type{};

#endif