# Require 3.1 for CMAKE_CXX_STANDARD property
cmake_minimum_required(VERSION 3.1)

project(PennaFishPopulation)

# C++11
set(CMAKE_CXX_STANDARD 11)

# Make it a Release build to add -O3 and -DNDEBUG flags by default
# Comment it out to revert to standard behaviour
set(CMAKE_BUILD_TYPE Release)
# Also add -march-native (and -funroll-loops if you switch lines)
add_compile_options($<$<CONFIG:RELEASE>:-march=native>)
# add_compile_options("$<$<CONFIG:RELEASE>:-march=native;-funroll-loops>")

# Setting warning compiler flags
if(CMAKE_CXX_COMPILER_ID MATCHES "(C|c?)lang")
  add_compile_options(-Weverything)
else()
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

set(DIRECTORIES src test)

foreach(directory ${DIRECTORIES})
  add_subdirectory(${directory})
endforeach(directory)

# This needs to be in both the top-level and the test CMake file.
enable_testing()

include_directories(${CMAKE_SOURCE_DIR}/src)

add_executable(penna_sim main.cpp)
target_link_libraries(penna_sim penna)

add_executable(penna_sim_vector main.cpp)
set_target_properties(penna_sim_vector PROPERTIES COMPILE_FLAGS "-DPENNA_VECTOR")
target_link_libraries(penna_sim_vector penna_vector)
