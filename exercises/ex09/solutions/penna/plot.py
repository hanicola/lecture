#!/usr/bin/env python
# -*- coding: utf-8 -*-

from numpy import *
import matplotlib
matplotlib.use("agg")
from matplotlib.pyplot import *
import sys
import os

for pop_name in ['population_list', 'population_vec']:
    pop = loadtxt(os.path.join('result', '{}.dat'.format(pop_name)))
    figure()
    plot(pop[:,0],pop[:,1],'x')
    xscale('linear')
    yscale('log')
    xlabel('year')
    ylabel('population size')
    savefig(os.path.join('result', '{}.png'.format(pop_name)))

figure()
gen = loadtxt(os.path.join('result', 'gene_histogram.dat'))
plot(gen)
ylim(ymin=0)
xlabel('genome position')
ylabel('bad fraction')
savefig(os.path.join('result', 'gene_histogram.png'))
