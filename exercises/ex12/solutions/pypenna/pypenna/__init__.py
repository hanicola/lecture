#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Programming Techniques for Scientific Simulations, HS 2020
# Exercise 12.4

from .population import *
from .animal import *
from .genome import *
from .fishing import *
