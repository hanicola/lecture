#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Demo():
    """
    This class serves as a demonstration between class variables,
    instance variables and local variables.
    """
    variable = 5

    def __init__(self, variable):
        self.variable = variable

    def __str__(self):
        variable = 15
        return (f'Class variable "Demo.variable": {Demo.variable}\n'
                f'Instance variable "self.variable": {self.variable}\n'
                f'Local variable "variable": {variable}\n'
                )


def main():
    demo = Demo(10)
    demo_2 = Demo(15)

    Demo.variable = 20

    print(demo)
    print(demo_2)


if __name__ == '__main__':
    main()
