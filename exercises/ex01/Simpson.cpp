# include <iostream>
# include <cmath>

// declaration
double function(double var);

int main(){
	int bins = 0;
	std::cout << "Give number of bins." << std::endl;
	std::cin >> bins;
// std::cout << bins << std::endl;

	double a = 0; // lower bound
	double b = M_PI; // upper bound

	double delta_x = (b-a)/bins;

	double result = 0; // initialise result to 0
	double x = a; // initialise x to lower bound
	result += function(a); // add first term

	// calculate result (part in brackets) iteratively
	for(int i=1; i<=bins; i++){
// std::cout << 4*function(x+delta_x/2) << std::endl;
		result += 4*function(x+delta_x/2);
		if (i != bins){ //für alle Fälle bis auf den letzten
// std::cout << 2*function(x+delta_x) << std::endl;
			result += 2*function(x+delta_x);
 		}
// std::cout << "x is: " << x << "Zwischenresultat: " << result << std::endl;
 	x += delta_x; // update x 
	}
	
	result += function(b); // add last term
	result *= (delta_x/6); // multiply with delta x / 6
	std::cout << result << std::endl;
}


//definition
// f(X) = 
double function(double x){
// std::cout << "function: " << x*(1-x) << std::endl;
	return sin(x);
}