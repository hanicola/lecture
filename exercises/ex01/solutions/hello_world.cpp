/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 1.3
 * 2020-09-24
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */
#include <iostream>

using namespace std;

int main() {
	cout << "Hello ETH students." << endl;
	return 0;
}
