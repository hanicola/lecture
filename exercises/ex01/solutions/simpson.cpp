/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 1.6
 * 2020-09-24
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <cmath>    // for std::sin
#include <iomanip>  // for std::setprecision
#include <iostream> // for std::cin, std::cout

double simpson(double lower_bound, double upper_bound, std::size_t nbins) noexcept;

double f(double x) noexcept {
	return std::sin(x);
}

int main() {
	constexpr double lower_bound = 0;
	constexpr double upper_bound = M_PI;
	std::size_t nbins = 0;

	std::cout << "Enter positive integer nbins:\n";
	std::cin >> nbins;

	std::cout
		<< std::setprecision(20)
		<< "integral of sin(x) from x = 0 to x = pi"                   << '\n'
		<< "approximate: " << simpson(lower_bound, upper_bound, nbins) << '\n'
		<< "exact:       " << 2.0                                      << '\n'
	;
}

double simpson(double lower_bound, double upper_bound, std::size_t nbins) noexcept {
	double integral = 0;
	double const delta = (upper_bound - lower_bound) / nbins;
	for (int i = 0; i < nbins; ++i) {
		integral += delta / 6 * (
			      f(lower_bound +  i        * delta)
			+ 4 * f(lower_bound + (i + 0.5) * delta)
			+     f(lower_bound + (i + 1)   * delta)
		);
	}

	return integral;
}
