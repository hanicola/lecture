#ifndef MY_ITERATOR_HPP
#define MY_ITERATOR_HPP
/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 7.1
 * 2020-10-29
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <cassert>
#include <iostream> // for debugging
#include <iterator> // for iterator category tags

/*
Forward & bidirectional iterators requirements:

Iterator:
- CopyConstructible
- CopyAssignable
- Destructible
- Supports: *a (Dereferenceable)
- Supports: ++a (Preincrementable)

Input Iterator:
- All requirements of an iterator.
- Supports: == (EqualityComparable)
- Supports: !=
- Supports: ->
- Supports: a++ (Postincrementable)

Forward Iterator:
- All requirements of an input iterator
- DefaultConstructible
- Supports expression: *a++

Bidirectional Iterator:
- All requirements of a forward iterator
- Predecrementable
- Postdecrementable
- Supports expression: *a--

*/

// my iterator
template <typename T>
class MyIterator {
  public:
    // member types
    using value_type = T; // type of values obtained when dereferencing the
                          // iterator
    using difference_type = std::size_t; // signed integer type to represent
                                         // distance between iterators
    using reference = T&; // type of reference to type of values
    using pointer = T*; // type of pointer to the type of values
    using iterator_category = std::forward_iterator_tag; // category of
                                                         // the iterator
    // using iterator_category = std::bidirectional_iterator_tag; // category of
    //                                                            // the iterator

    // TODO: constructor

    // copy ctor
    MyIterator(MyIterator const&) = default;
    // copy assignment
    MyIterator& operator=(MyIterator const&) = default;
    // dtor
    ~MyIterator() { }

    // TODO: operators

  private:

    // TODO: private members
    // TODO: private method "check" to prevent illegal operations
};

#endif /* MY_ITERATOR_HPP */
