/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 7.2
 * 2020-11-05
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include "timer.hpp"

Timer::Timer() {
  tstart_ = std::chrono::high_resolution_clock::now();
}

void Timer::start() {
  tstart_ = std::chrono::high_resolution_clock::now();
}

void Timer::stop() {
  tend_ = std::chrono::high_resolution_clock::now();
}

double Timer::duration() const {
  return std::chrono::duration< double >(tend_ - tstart_).count();
}
