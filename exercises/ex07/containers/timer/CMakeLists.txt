# Programming Techniques for Scientific Simulations I
# HS 2020
# Exercise 7.2
# 2020-10-29
# Michal Sudwoj <msudwoj@student.ethz.ch>

cmake_minimum_required(VERSION 3.1)
project(timer)

set(CMAKE_CXX_STANDARD 11)

add_library(timer STATIC timer.cpp)

install(TARGETS timer DESTINATION lib)
install(FILES timer.hpp DESTINATION include)
