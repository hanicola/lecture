#!/usr/bin/env python
# Programming Techniques for Scientific Simulations I
# HS 2020
# Exercise 7.2
# 2020-10-29
# Michal Sudwoj <msudwoj@student.ethz.ch>

from argparse import ArgumentParser
import pandas as pd
import matplotlib.pyplot as plt


def main():
    parser = ArgumentParser()
    parser.add_argument("input_file")
    parser.add_argument("output_file", nargs="?")
    args = parser.parse_args()

    data = pd.read_fwf(args.input_file).drop("#", "columns").set_index("N")

    fig = plt.figure()
    fig.gca().set_xscale("log", base=2)
    fig.gca().set_yscale("log", base=10)
    for container in ["Vector", "List", "Set"]:
        plt.plot(data.index, data[container + "[ns/op]"], label=container)
    plt.legend()
    plt.title("Standard container comparison")
    plt.xlabel("Size in elements")
    plt.ylabel("Time in ns / op")

    if args.output_file:
        plt.savefig(args.output_file)
    else:
        plt.show()


if __name__ == "__main__":
    main()
