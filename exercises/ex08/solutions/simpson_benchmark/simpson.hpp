#ifndef SIMPSON_INTEGRATION_HPP
#define SIMPSON_INTEGRATION_HPP
/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 8.3
 * 2020-11-12
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <stdexcept>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////
// Hard-coded version
////////////////////////////////////////////////////////////////////////////////
double simpson_hardcoded(const double a,
                         const double b,
                         const unsigned bins);


////////////////////////////////////////////////////////////////////////////////
// Function pointer version
////////////////////////////////////////////////////////////////////////////////
double simpson_function_ptr(double (*func)(double),
                            const double a,
                            const double b,
                            const unsigned bins);


////////////////////////////////////////////////////////////////////////////////
// Templated function object version (compile-time polymorphism)
////////////////////////////////////////////////////////////////////////////////
template <typename F>
double simpson_templated_fo(const F& func,
                            const double a,
                            const double b, 
                            const unsigned bins) {
    // std::cout << "Templated function object version" << std::endl;

    using argument_t = typename F::argument_type;
    using result_t   = typename F::result_type;

    if (bins == 0)
        throw std::logic_error("Simpson_integrate(..) : Number of bins has to be positive.");
    
    const argument_t dr = (b - a) / static_cast<argument_t>(2*bins);
    result_t I2(0), I4(0);
    argument_t pos = a;
    
    for (unsigned int i = 0; i < bins; ++i) {
        pos += dr;
        I4 += func(pos);
        pos += dr;
        I2 += func(pos);
    }

    return (func(a) + 2.*I2 + 4.*I4 - func(b)) * (dr/3.);
}


////////////////////////////////////////////////////////////////////////////////
// Abstract base class version (runtime polymorphism)
////////////////////////////////////////////////////////////////////////////////
struct Function {
    using result_type = double;
    using argument_type = double;

    virtual result_type operator()(const argument_type x) const = 0;
    virtual ~Function() {}
};

Function::result_type simpson_base_class(const Function& func,
                                         const Function::argument_type a,
                                         const Function::argument_type b,
                                         const unsigned bins);

#endif  // SIMPSON_INTEGRATION_HPP
