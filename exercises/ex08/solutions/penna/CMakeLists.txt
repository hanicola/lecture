# Require 3.1 for CMAKE_CXX_STANDARD property
cmake_minimum_required(VERSION 3.1)

# Make it a Release build by default to add -O3 and -DNDEBUG
# Can be overridden from command line or GUI
# This code adapted from: https://blog.kitware.com/cmake-and-the-default-build-type/ 
set(default_build_type "Release")
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
      STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

project(PennaFishPopulation)

# C++11
set(CMAKE_CXX_STANDARD 11)

# For Release, add -march-native (and -funroll-loops if you switch lines)
add_compile_options($<$<CONFIG:RELEASE>:-march=native>)
# add_compile_options("$<$<CONFIG:RELEASE>:-march=native;-funroll-loops>")

# Setting warning compiler flags
if(CMAKE_CXX_COMPILER_ID MATCHES "(C|c?)lang")
  add_compile_options(-Weverything)
else()
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

set(DIRECTORIES src test)

foreach(directory ${DIRECTORIES})
  add_subdirectory(${directory})
endforeach(directory)

# This needs to be in both the top-level and the test CMake file.
enable_testing()

add_executable(penna_sim main.cpp)
target_link_libraries(penna_sim penna)
