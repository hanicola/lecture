#include <iostream>


template <typename T>
	T epsilon (){
		T e = 1;
		while (( T{1.0} + T{0.5}*e) != T{1.0}){
			e *= T{0.5}*e;
		}
		return e;
	}

int main(){
	float epsilon_float = epsilon <float>();
	double epsilon_double = epsilon <double>();
	long double epsilon_ldouble = epsilon <long double>();
	
	std::cout << "The machine epsilon is: \n" 
	<< "in float:		" << epsilon_float << 
	"\nin double:		" << epsilon_double << 
	"\nin long double:		" << epsilon_ldouble << std::endl;
	
}
