#include <iostream>
#include <vector>
//using namespace std;

int main(){
// how many numbers are there to read in?
int n;
std::cout << "Please give a positive integer." << std::endl;
std::cin >> n;

//static array
double arr[n];

// read in input & calculate sum
double sum;
sum = 0;
for(int i=0; i<n; ++i){
  std::cin >> arr[i];
  sum += arr[i];
}

/*
// normalise
for(int i=0; i<n; i++){
  arr[i] = arr[i]/sum;
}

// output in reverse order
for (int i=n-1; i>=0; --i){
  std::cout << arr[i] << ", " << std::endl; 
}
*/

//output in reverse order & normalisation
for (int i=n-1; i>=0; --i){
  std::cout << arr[i]/sum << ", " << std::endl; 
}


// dynamic array
int m;
std::cout << "Please give a positive integer." << std::endl;
std::cin >> m;

double number;
double sumd;
sumd = 0;
std::vector<double> arrd;
for (int j=0; j<m; j++){
  std::cin >> number;
  arrd.push_back(number);
  sumd += number;
}

//output in reverse order & normalisation
for(int k=0; k<m; k++){
  std::cout << arrd.back()/sumd << ", " <<std::endl;
  arrd.pop_back();
}

//


} // end main