# include <iostream>
# include <cmath>
# include "Simpson.hpp"

// declaration
double function(double var);

int main(){
	int bins = 0;
	std::cout << "Give number of bins." << std::endl;
	std::cin >> bins;

	double a = 0; // lower bound
	double b = M_PI; // upper bound
	
	double result = Simpsonfunction(function, a, b, bins);
	std::cout << result << std::endl;
}


//definition
// f(X) = sin(x)
double function(double x){
	return sin(x);
}