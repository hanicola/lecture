/*
does the integration
*/

#include <cassert>
#include "Simpson.hpp"

double Simpsonfunction(function_type function, double a, double b, int bins){
	//assertions to check validity of input
	assert( bins > 0);
	assert (function != nullptr);
	
	double delta_x = (b-a)/bins;

	double result = 0; // initialise result to 0
	double x = a; // initialise x to lower bound
	result += function(a); // add first term

	// calculate result (part in brackets) iteratively
	for(int i=1; i<=bins; i++){
		result += 4*function(x+delta_x/2);
		if (i != bins){ //für alle Fälle bis auf den letzten
			result += 2*function(x+delta_x);
 		}
 	x += delta_x; // update x 
	}
	
	result += function(b); // add last term
	result *= (delta_x/6); // multiply with delta x / 6
	return result;
}