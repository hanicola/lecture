# #comment
# target: prerequesites
#[TAB] commands

.PHONY: all
all: Simpsonintegration

Simpsonfunction.o: Simpsonfunction.cpp Simpson.hpp
	c++ -c Simpsonfunction.cpp
	
Simpson.o: Simpson.cpp Simpson.hpp
	c++ -c Simpson.cpp
	
Simpsonintegration: Simpson.o libintegrate.a
	c++ -o Simpsonintegration Simpson.o -L. -lintegrate

libintegrate.a : Simpsonfunction.o
	ar -crs libintegrate.a Simpsonfunction.o