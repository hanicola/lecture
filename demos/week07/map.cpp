#include <iostream>
#include <map>
#include <string>

int main() {

  std::map<std::string, long> phone_book;
  phone_book["Kaeppeli"] = 791234567;
  phone_book["Mueller"]  = 781234567;

  std::string name = "Muller";
  if( phone_book[name] ) {
    std::cout << "The phone number of " << name <<  " is "
              << phone_book[name];
  } else {
    std::cout << name << "'s phone number is unknown!\n";
  }

}

