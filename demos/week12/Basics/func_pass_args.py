def incr(x):
    x += 1

x = 0
incr(x)
print(x)

def incr_first(x):
    x[0] += 1

x = [0,1,2]
incr_first(x)
print(x)

