class Point:
    """A simple two-dimensional Cartesian coordinates point"""

    dim = 2 # static / class variable

    def __init__(self, x, y):
        """Create point from two Cartesian coordinates"""
        self._x = x # leading underscore for
        self._y = y # private stuff convention

    @staticmethod
    def dimensions():
        return Point.dim

    @classmethod
    def name(cls):
        return cls.__name__

    @property
    def x(self):
        """The point's x coordinate"""
        return self._x
    @x.setter       
    def x(self, val):
        self._x = val

    @property
    def y(self):
        """The point's y coordinate"""
        return self._y
    @y.setter       
    def y(self, val):
        self._y = val

    def scale(self, sx=1, sy=1):
        """Scale the point by a factors in x- and y-direction"""
        self._x *= sx
        self._y *= sy

    def __add__(self, p):
        """Add to points"""
        return Point(self._x + p._x, self._y + p._y)

    def __str__(self):
        """Convert a point to string (e.g. for pretty-printing)"""
        return "(" + str(self._x) + "," + str(self._y) + ")"

if __name__ == "__main__":
    P1 = Point(1., 2.)
    P2 = Point(0 , 7 )
    print("P1 =", P1)
    print("P2 =", P2)
    P2.y = 2 # set P2.y to 2!
    print("P2 =", P2)
    print("P1 + P2 =", P1 + P2)

    print(Point.dimensions())

