5*7   # 35
5/2   # 2.5 floating point division
5//2  # 2 floor division
5 % 2 # 1 modulo operator
(17 - 3)/(4 - 5/3)
3**4  # power operator
1j**2
import math   # import math. functions module
math.sqrt(2.)
math.sin(math.pi)
import random # import rand. numb. gen.
random.random()

