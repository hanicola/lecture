cmake_minimum_required(VERSION 2.8)

project(week06)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)          # set standard to C++11
set(CMAKE_CXX_STANDARD_REQUIRED ON) # requires C++11
set(CMAKE_CXX_EXTENSIONS OFF)       # disables extensions such as -std=g++XX


# setting warning compiler flags
if(CMAKE_CXX_COMPILER_ID MATCHES "(C|c?)lang")
  add_compile_options(-Weverything)
else()
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

add_executable(exceptional exceptional.cpp)
add_executable(hello_date hello_date.cpp)
add_executable(fibo fibo.cpp)
add_executable(timed_loop timed_loop.cpp)
add_executable(random_engines random_engines.cpp)
add_executable(random_normal random_normal.cpp)

