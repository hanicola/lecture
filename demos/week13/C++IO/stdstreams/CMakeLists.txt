cmake_minimum_required(VERSION 2.8)

project(week13)

# C++11
set(CMAKE_CXX_STANDARD 11)

foreach(I 1 2 3)
  add_executable(stdstream${I} stdstream${I}.cpp)
endforeach(I)

