#!/bin/bash
#BSUB -W 00:30
#BSUB -n 10
#BSUB -J mpi4py
(time -p mpirun python mpi_hello.py) 2>&1 | tee mpi4py.log
exit

